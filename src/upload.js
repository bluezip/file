import express from 'express'
import fs from 'fs-extra'
import mime from 'mime'
import multer from 'multer'
import path from 'path'

import { FOLDER_UPLOAD, BASE_URL, ENV } from './lib/config'
import { handle_error, random_name } from './lib/utile'
import { isAuth } from './lib/auth'
import db from './lib/db'

const Router = express.Router()

const upload = multer({
  dest: 'tmp/',
  fileFilter: (req, file, cb) => {
    const filetypes = /jpe?g|jpg|gif|mp4|avi/;
    const mimetype = filetypes.test(file.mimetype);
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    if (mimetype && extname) {
      return cb(null, true);
    }
    cb({ message: "Error: File upload only supports the following filetypes - " + filetypes });
  }
})


Router.post('/', upload.single('file'), isAuth, (req, res, next) => {
  if (!req.file) return handle_error(res, { message: 'Please upload file', code: 400 })

  const name = random_name(Math.floor(Math.random() * 4) + 4) + "." + mime.getExtension(req.file.mimetype)
  const folder = `${random_name(2)}/${random_name(2)}`
  const new_path = `${FOLDER_UPLOAD}/${folder}/${name}`

  if (fs.existsSync(new_path)) {
    return handle_error(req, { message: `File ${new_path} not exists` })
  }

  fs.move(req.file.path, new_path)
    .then(data => {
      return new Promise((resolve, reject) => {
        fs.remove(req.file.path, (err) => {
          if (err) return reject()
          return resolve(data)
        })
      })
    })
    .then(data => {
      db.insert({
        size: req.file.size,
        type: /jpe?g|jpg|gif/.test(req.file.mimetype) ? "photo" : "video",
        mimetype: req.file.mimetype,
        path: `/${folder}/${name}`
      }, (err, data)=> {
        res.setHeader('Content-Type', 'application/json')
        res.send({
          mimetype: req.file.mimetype,
          extension: mime.getExtension(req.file.mimetype),
          original_name: req.file.originalname,
          size: req.file.size,
          path: `/${folder}/${name}`,
          type: /jpe?g|jpg|gif/.test(req.file.mimetype) ? "photo" : "video",
          full_path: `${BASE_URL}/${folder}/${name}`,
          base_url: BASE_URL
        })
      })
    })
    .catch(err => {
      return handle_error(res, { message: error })
    })
})

Router.delete('/', isAuth, (req, res, next) => {
  if (!req.query.path || !/^\//.test(req.query.path))  return handle_error(res, { message: "File not find", code: 404 })

  const file = path.join(__dirname, '..', FOLDER_UPLOAD, req.query.path)
  if (!fs.existsSync(file)) return handle_error(res, { message: "File not find", code: 404 })

  try {
    db.remove({ path: req.query.path }, (err) => {
      if(err) throw new Error(err)
      fs.removeSync(file)
      res.sendStatus(204)
    })
  } catch (err) {
    return handle_error(res, { message: err })
  }
})

export default Router