export const PORT = process.env.PORT || 8080
export const ENV = process.env.NODE_ENV || 'development'
export const IP = process.env.IP || "127.0.0.1"

export const SECRETS = {
  SESSION: 'tkXw64$&eeaGUG165/=077',
  EXPIRES: '6h'
}

export const FOLDER_UPLOAD = './uploads'

export const AUTH = {
  username: "admin",
  password: "112233"
}

export const BASE_URL = `http://${IP}:${PORT}`