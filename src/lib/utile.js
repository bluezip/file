export const random_name = (max = 5) => {
  var text = "";
  var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < max; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

export const handle_error = (res, { message, code = 500 }) => {
  res.statusCode = code;
  res.send({
    name: "Error",
    message
  });
}

export const handle_not_allowed = (res) => {
  handle_error(res, { 
    message: "Not allowed",
    code: 401 
  })
}
