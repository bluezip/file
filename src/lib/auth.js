import express from 'express'
import jwt from 'jsonwebtoken'

import { AUTH, SECRETS } from './config'
import { handle_error, handle_not_allowed } from './utile'

const Router = express.Router()

Router.post('/', (req, res, next) => {
  if (req.body.username === AUTH.username && req.body.password === AUTH.password) {
    const token = jwt.sign({
      login: true
    }, SECRETS.SESSION, { expiresIn: SECRETS.EXPIRES })
    res.send({ token: token })
  } else {
    return handle_error(res, { message: "!Authtencation", code: 401 })
  }
})

export const auth = Router
export const isAuth = (req, res, next) => {
  if (req.query && req.query.hasOwnProperty('access_token')) {
    req.headers.authorization = 'Bearer ' + req.query.access_token
  }
  if (!req.headers.authorization) {
    return handle_error(res, { message: "invalid token", code: "401" })
  } else {
    try {
      const token = req.headers.authorization.split(" ")[1]
      const decoded = jwt.verify(token, SECRETS.SESSION)
      return next()
    } catch (err) {
      return handle_error(res, { message: "invalid token", code: "401" })
    }
  }
}