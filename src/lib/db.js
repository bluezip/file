import Datastore from 'nedb'
import path from 'path'

import { ENV } from './config'

let db
if (ENV.match(/production/i)) {
  db = new Datastore({
    filename: path.join(__dirname, '../..', 'db', 'data.db'), autoload: true
  })
} else {
  db = new Datastore({
    filename: path.join(__dirname, '../..', 'db', 'test.db'), autoload: true
  })
}

module.exports = db