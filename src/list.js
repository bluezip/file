import express from 'express'

import db from './lib/db'

const Router = express.Router()

Router.get('/', (req, res, next) => {
  db.find({}, (err, data) => {
    if(err) return res.send(err)
    res.send(data)
  })
})

export default Router