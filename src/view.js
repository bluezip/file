import express from 'express'
import fs from 'fs'
import jwt from 'jsonwebtoken'
import mime from 'mime'
import path from 'path'

import { AUTH, SECRETS } from './lib/config'
import { handle_error } from './lib/utile'

const Router = express.Router()

const video = (file, req, res) => {
  try {
    const token = req.query.access_token
    const decoded = jwt.verify(token, SECRETS.SESSION)
    // FIXME: check ip (not test)
    if ((decoded.ip && decoded.ip === req.ip) || req.ip === "::1") {

      const range = req.headers.range || "0"
      const positions = range.replace(/bytes=/, "").split("-")
      const start = parseInt(positions[0], 10)

      fs.stat(file, function (err, stats) {
        const total = stats.size;
        const end = positions[1] ? parseInt(positions[1], 10) : total - 1;
        const chunksize = (end - start) + 1;

        res.writeHead(206, {
          "Content-Range": "bytes " + start + "-" + end + "/" + total,
          "Accept-Ranges": "bytes",
          "Content-Length": chunksize,
          "Content-Type": mime.getType(file)
        })

        const stream = fs.createReadStream(file, { start: start, end: end })
          .on("open", function () {
            stream.pipe(res)
          }).on("error", function (err) {
            res.end(err)
          })
      })
    } else {
      return handle_error(res, { message: "!Authentication", code: 401 })
    }
  } catch (e) {
    return handle_error(res, { message: "!Authentication", code: 401 })
  }
}

const photo = (file, req, res) => {
  res.writeHead(200, {
    "Content-Type": mime.getType(file)
  })
  const stream = fs.createReadStream(file)
    .on("open", function () {
      stream.pipe(res)
    }).on("error", function (err) {
      res.end(err)
    })
}

Router.get('/(*)', (req, res, next) => {
  const file = path.join(__dirname, '..', 'uploads', req.params[0])

  if (!fs.existsSync(file)) {
    return handle_error(res, { message: "Not find", code: 404 })
  }

  if (/jpe?g|gif|png/.test(mime.getType(file))) {
    return photo(file, req, res)
  }

  if (/mp4|avi/.test(mime.getType(file))) {
    return video(file, req, res)
  }

  return handle_error(res, { message: "Not find", code: 404 })

})
export default Router