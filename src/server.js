import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'

import { PORT, IP } from './lib/config'
import { auth, isAuth } from './lib/auth'
import list from './list'
import upload from './upload'
import view from './view'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(cors())
app.set('trust proxy', true)

app.use('/auth', auth)
app.use('/upload', upload)
app.use('/list', isAuth, list)
app.use(view)

app.listen(PORT, (err) => {
  if (err) return console.error(err)
  console.log(`http://${IP}:${PORT}`)
})

module.exports = app