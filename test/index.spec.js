import assert from 'assert'
import path from 'path'
import request from 'supertest'

import { AUTH } from '../src/lib/config'
import app from '../src/server'
import db from '../src/lib/db'

let token = null
let file = null
describe('/auth', () => {
  it('should return a status of 401', done => {
    request(app)
      .post('/auth')
      .expect('Content-Type', /json/)
      .expect(401, done)
  })

  it('should return a status of 401', done => {
    request(app)
      .post('/auth')
      .send({ username: 'admin', password: 'admin' })
      .expect('Content-Type', /json/)
      .expect(401, done)
  })

  it('should return a status of 200', done => {
    request(app)
      .post('/auth')
      .send({ username: AUTH.username, password: AUTH.password })
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        assert(res.body.token.length > 20, true)
        token = res.body.token
        done()
      })
  })
})

describe('/upload', () => {
  before((done) => {
    db.remove({}, { multi: true }, done)
  })
  it('should return 401', (done) => {
    request(app)
      .post('/upload')
      .expect('Content-type', /json/)
      .expect(401, done)
  })

  it('should return 400', (done) => {
    request(app)
      .post('/upload')
      .set('Authorization', `Bearer ${token}`)
      .attach('file', path.join(__dirname, 'test.jpg'))
      .expect('Content-type', /json/)
      .expect(200)
      .end((err, data) => {
        if (err) return done(err)
        file = data.body
        return done()
      })
  })

  it('should return 401', (done) => {
    request(app)
      .get('/list')
      .expect(401, done)
  })

  it('should return array', (done) => {
    request(app)
      .get('/list')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .expect('Content-type', /json/)
      .end((err, data) => {
        assert(data.body.length === 1, true)
        done()
      })
  })

  it('should return 404', (done) => {
    request(app)
      .delete(`/upload?file=${file.path}`)
      .set('Authorization', `Bearer ${token}`)
      .expect(404, done)
  })

  it('check images should return 200', (done) => {
    request(app)
      .get(`${file.path}`)
      .expect(200, done)
  })

  it('should return 204', (done) => {
    request(app)
      .delete(`/upload?path=${file.path}`)
      .set('Authorization', `Bearer ${token}`)
      .expect(204, done)
  })

  it('should return array (0)', (done) => {
    request(app)
      .get('/list')
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .expect('Content-type', /json/)
      .end((err, data) => {
        assert(data.body.length === 0, true)
        done()
      })
  })
})
